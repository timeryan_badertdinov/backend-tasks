﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BackendTasks
{
    public class StudentsGrade
    {
        public StudentsGrade(string student, string subject, int grade)
        {
            this.Student = student;
            if (subject == "История" || subject == "Математика" || subject == "География")
                this.Subject = subject;
            else this.Subject = "Не указан";
            if (grade >= 2 && grade <= 5)
                this.Grade = grade;
            else this.Grade = 0;
        }
        public string Student { get; set; }
        public string Subject { get; set; }
        public int Grade { get; set; }

        public override string  ToString()
        {
            return String.Format("{0} {1} {2}", Student, Subject, Grade);
        }

    }
}
