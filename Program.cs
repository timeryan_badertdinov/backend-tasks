﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BackendTasks
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] subjects = { "История", "Математика", "География" };
            Random random = new Random();
            var studentGrades = new List<StudentsGrade>();

            for (int i = 0; i < 100; i++) {
                studentGrades.Add( new StudentsGrade(random.Next(1000, 9999).ToString(), subjects[random.Next(0, 3)], random.Next(2, 6)));
           
            }

            var selectedStudents = from s in studentGrades
                                        where s.Subject == "История" && s.Grade == 5
                                        select s.Student;


            var selectedStudentGrades = from s in studentGrades
                                        where s.Subject == "География" && s.Grade == 2
                                        orderby s.Student
                                        select s;
            ;

            foreach (var s in studentGrades)
                Console.WriteLine(s.ToString());

            foreach (var s in selectedStudents)
                Console.WriteLine(s);

            foreach (var s in selectedStudentGrades)
                Console.WriteLine(s.ToString());
        }     
        
        }
}